TARGET=libbindp.so
INSTALL_PATH=/usr/local/lib/

all:
	gcc -nostartfiles -fpic -shared bindp.c -o ${TARGET} -ldl -D_GNU_SOURCE

install:
	@echo setting up...
	cp ./${TARGET} ${INSTALL_PATH}
	chmod +x ${INSTALL_PATH}

clean:
	rm ${TARGET} -f
